from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from .. import db, login_manager


class User (UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    password_hash = db.Column(db.String(128))

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, passwd):
        self.password_hash = generate_password_hash(passwd)
    
    def verify_password(self, passwd):
        return check_password_hash(self.password_hash, passwd)

    def __repr__(self):
        return '<User %r>' % self.username

@login_manager.user_loader
def load_user (user_id):
    return User.query.get(int(user_id))
    

from . import auth

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, BooleanField
from wtforms.validators import Length, Required, EqualTo


class LoginForm (FlaskForm):
    username = StringField('Username: ',
            validators=[Required()]
    )
    password = PasswordField('Password: ',
            validators=[Required()]
    )
    submit = SubmitField('Login')


class UserAddForm (FlaskForm):
    username = StringField('Username: ',
            validators=[Required(), Length(max=100)]
    )
    password = PasswordField('Password: ',
            validators=[Required(), Length(max=100)]
    )
    confirm_password = PasswordField('Confirm: ',
            validators=[Required(), Length(max=100), EqualTo(password)]
    )
    submit = SubmitField('Register')


class MainMenuForm (FlaskForm):
    led5on = BooleanField('LED 5 On?')
    submit = SubmitField()

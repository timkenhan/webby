from flask import render_template, redirect, flash, url_for, request
from flask_login import login_user, logout_user

from . import auth
from .forms import LoginForm
from ..models.user import User


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm ()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and user.verify_password(form.password.data):
            login_user(user)
            return redirect(request.args.get('next') or url_for('main.index'))
        flash('Invalid login!')
    return render_template ('auth/login.html', form=form)


@auth.route('/logout')
def logout():
    logout_user()
    flash('Logged out!')
    return redirect(url_for('main.index'))

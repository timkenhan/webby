from flask import render_template, session
from flask_login import current_user
from io import UnsupportedOperation
import json

from . import main

from .. import db
from ..models.user import User
from ..auth.forms import UserAddForm, MainMenuForm



@main.route('/', methods=['GET', 'POST'])
def index():
    form = MainMenuForm ()
    if current_user.is_authenticated:
        return render_template ('user_index.html', form=form)
    return render_template ('anon_index.html')


@main.route('/user_add', methods=['GET', 'POST'])
def user_add():
    form = UserAddForm()
    if form.validate_on_submit():
        new_user = User(username = form.username.data)
        new_user.password = form.password.data
        form.password.data = ''
        return redirect (url_for('index'))
    return render_template ('user_add.html',
            form=form, name=session.get('name'))


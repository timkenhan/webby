import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    WTF_CSRF_ENABLED = True
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'blablablah'
    if os.environ.get('DATABASE_URL') is None:
        SQLALCHEMY_DATABASE_URI='sqlite:///'+os.path.join(basedir,'data.sqlite')
    else:
        SQLALCHEMY_DATABASE_URI=os.environ['DATABASE_URL']

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig (Config):
    DEBUG = True
    
class ProductionConfig(Config):
    @classmethod
    def init_app(app):
        # log to syslog
        import logging
        from systemd.journal import JournalHandler
        journal_handler = logging.getLogger('webby')
        journal_handler.addHandler(SysLogHandler())
        syslog_handler.setLevel(logging.WARNING)
        app.logger.addHandler(journal_handler)

config = { 'default': Config}

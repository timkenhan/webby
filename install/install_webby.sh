#!/bin/bash

if !(USER = root); then
    exit
fi

if !(-e /usr/bin/lighttpd)
    then apt-get install lighttpd
fi

cp lighttpd.conf /etc/lighttpd/
cp -a webby /srv

#!flask/bin/python
import os
from flipflop import WSGIServer
from app import create_app


app = create_app(os.getenv('FLASK_CONFIG') or 'default')

if __name__ == '__main__':
    WSGIServer(app).run()


